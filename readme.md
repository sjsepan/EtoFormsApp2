# readme for EtoFormsApp2

## Desktop GUI app prototype, on Linux(/Win/Mac), in C# / DotNet[5|6] / Gtk\#(/WPF/AppKit), using VSCode / Eto.Forms

![EtoFormsApp2_Gtk.png](./EtoFormsApp2_Gtk.png?raw=true "Screenshot")

### About

Although I had already done a Gtk# app (GtkSharpApp1), this platform is  different enough, mature enough, and popular enough to warrant coverage. I investigated Eto.Forms a year or two ago when I was still using Win7 in VirtualBox, but did not follow through because the forms designer tooling for Visual Studio did not seem to work.
I decided to give it another look because it keeps getting mentions on social media, and because it truly generates cross-platform builds. The lack of a form designer is kind of moot on VSCode, where I have done projects for other platforms without one either, so I figured why not. Note that is is EtoFormsApp2 because I had already started a project with the default XAML code-behind option. More on that below...
With Eto.Forms, it is worth noting that 1) there are now four options for the code-behind/side-car arrangement of form-layout/form-code and 2) as of 2.5 'net5.0' is supported.
For the latter, I am using the current release version 'net6.0'.
For the former (xml, json, single-file code class, partial-class code), I have chosen the partial-class code-only implementation ('preview') because...

1) it is closest to the way that Visual Studio seemed to do things, and
2) intellisense on the Eto classes is available for code-only, whereas none is available for at least two of the other methods (xaml, json).

Regarding the partial-class implementation, it is worth noting another factor in my decision: there is a push on to create a designer (<https://github.com/dsoronda/EtoDesigner>) although it hasn't moved for the last six months. However, I will be refactoring and coding MainForm.eto.cs to appear like the output of the WinForms designer (<FormName>.designer.cs) as an example of what I would hope that project would consume / produce.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Install Eto.Forms templates for .Net Core

dotnet new --install Eto.Forms.Templates
Add <https://www.nuget.org/packages/Eto.Forms.Templates/>
Adds etoapp, etofile

### Getting Started

<https://github.com/picoe/Eto/wiki/Quick-Start>
dotnet new etoapp -sln -m xaml
dotnet new etoapp -sln -m json
dotnet new etoapp -sln -m code
dotnet new etoapp -sln -m preview

### Linux (Ubuntu): Get System.Drawing.Common features

sudo apt install libc6-dev
sudo apt install libgdiplus

### Build / Run from terminal

If you try to run ('dotnet run') from EtoFormsApp2 project folder, you will get an error "The current OutputType is 'Library'". To run EtoFormsApp2.Gtk, for example, make sure you run from the EtoFormsApp2.Gtk folder.

### Tutorials

<https://github.com/picoe/Eto/wiki/Tutorials>
<https://www.hanselman.com/blog/crossplatform-guis-with-open-source-net-using-etoforms>

### Notes

Note: adding a command directly to ToolBar instead of adding a ButtonToolItem using that command gives warning "(EtoFormsApp2.Gtk:29033): GLib-CRITICAL **: 10:57:46.633: Source ID 11 was not found when attempting to remove it"
<https://stackoverflow.com/questions/23199699/glib-critical-source-id-xxx-was-not-found-when-attempting-to-remove-it>
<https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/1264368>

### History

0.0.3:
-update projects to net8.0
-update Eto.Forms, Eto.Platform.* nuget to 2.8.2
~apply analyzer hints

0.0.2:
-update projects to net7.0
-update Eto.Platform.* nuget to 2.7.4

0.0.1:
-initial release

Steve Sepan
<sjsepan@yahoo.com>
3-9-2024
