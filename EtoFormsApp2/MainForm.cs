using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;

namespace EtoFormsApp2
{
	public partial class MainForm : Form
    {
        private const string ACTION_INPROGRESS = " ...";
        private const string ACTION_DONE = " done";
        private const string IMAGE_RESOURCE_PATH = "../EtoFormsApp2/Resources/{0}.png";

        Dictionary<string, Image> imageResources;

        public MainForm()
        {
            InitializeResources();
            InitializeComponent();
        }

        private void InitializeResources()
        {
            imageResources =
                new Dictionary<string, Image>()
                { //TODO:ideally, should get these from resource, but items must be generated into resource class.
                    { "App", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"App")), 32, 32) },
                    { "New", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"New")), 22, 22) },
                    { "Open", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Open")), 22, 22) },
                    { "Save", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Save")), 22, 22) },
                    { "Print", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Print")), 22, 22) },
                    { "Undo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Undo")), 22, 22) },
                    { "Redo", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Redo")), 22, 22) },
                    { "Cut", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Cut")), 22, 22) },
                    { "Copy", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Copy")), 22, 22) },
                    { "Paste", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Paste")), 22, 22) },
                    { "Delete", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Delete")), 22, 22) },
                    { "Find", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Find")), 22, 22) },
                    { "Replace", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Replace")), 22, 22) },
                    { "Refresh", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Reload")), 22, 22) },
                    { "Preferences", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Preferences")), 22, 22) },
                    { "Properties", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Properties")), 22, 22) },
                    { "Contents", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"Contents")), 22, 22) },
                    { "About", GetIconFromBitmap(GetBitmapFromFile(string.Format(IMAGE_RESOURCE_PATH,"About")), 22, 22) }
                };
        }

        #region Handlers
        //Event Handlers 

        private void MainForm_Closing(object sender, EventArgs e)
        {
            bool resultCancel = false;

            FileQuitAction(ref resultCancel);
            Console.WriteLine("cancel:" + resultCancel);

            ((System.ComponentModel.CancelEventArgs)e).Cancel = resultCancel;

            if (!resultCancel)
            {
                //clean up data model here

                //Application.Instance.Quit();//this will only re-trigger the Closing event; just allow window to close
            }
        }

        private void Any_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this, "I was clicked!");
            statusMessage.Text = "I was clicked! " + DateTime.Now.ToLongTimeString();
        }
        private void CmdColor_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Color" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Color"); 
            ColorAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void CmdFont_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Font" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Font"); 
            FontAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuFileNew_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "New" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("New");
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuFileOpen_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Open" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Open");
            FileOpenAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuFileSave_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Save" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Save");
            FileSaveAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuFileSaveAs_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Save As" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("SaveAs"); 
            FileSaveAsAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuFilePrint_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Print" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Print");
            FilePrintAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuFilePrintPreview_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Print Preview" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("PrintPreview"); 
            await FilePrintPreviewAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuFileQuit_Click(object sender, EventArgs e)
		{
            Close();
        }
        private async void MenuEditUndo_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Undo" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Undo");
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditRedo_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Redo" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Redo");
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Select All" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("SelectAll"); 
            await EditSelectAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCut_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Cut" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Cut");
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCopy_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Copy" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Copy");
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPaste_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Paste" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Paste");
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Paste Special" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("PasteSpecial"); 
            await EditPasteSpecialAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditDelete_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Delete" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Delete");
            await EditDeleteAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditFind_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Find" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Find");
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditReplace_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Replace" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Replace");
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditRefresh_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Refresh" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Refresh");
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuEditPreferences_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Preferences" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("Preferences");
            EditPreferencesAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowNewWindow_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "New Window" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("NewWindow"); 
            await WindowNewWindowAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowTile_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Tile" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Tile"); 
            await WindowTileAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowCascade_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Cascade" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Cascade"); 
            await WindowCascadeAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowArrangeAll_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Arrange All" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("ArrangeAll"); 
            await WindowArrangeAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowHide_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Hide" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Hide"); 
            await WindowHideAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowShow_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Show" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Show"); 
            await WindowShowAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpContents_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Contents" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Contents"); 
            await HelpContentsAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpIndex_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "Index" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("Index"); 
            await HelpIndexAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpOnlineHelp_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "OnlineHelp" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("OnlineHelp"); 
            await HelpOnlineHelpAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpLicenseInformation_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "LicenseInformation" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("LicenseInformation"); 
            await HelpLicenseInformationAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpCheckForUpdates_Click(object sender, EventArgs e)
		{
            statusMessage.Text = "CheckForUpdates" + ACTION_INPROGRESS;
            StartProgressBar();
            // StartActionIcon("CheckForUpdates"); 
            await HelpCheckForUpdatesAction();
            // StopActionIcon(); 
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            statusMessage.Text = "About" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon("About");
            HelpAboutAction();
            StopActionIcon();
            StopProgressBar();
            statusMessage.Text += ACTION_DONE;
        }
        #endregion Handlers

        #region Actions
        // Actions

        private static async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //progressBar.Pulse();
                //DoEvents;
                await Task.Delay(1000);
            }
        }

        private static async Task FileNewAction()
        {
            await DoSomething();
        }
        private void FileOpenAction()
        {
			try
			{
				OpenFileDialog openFileDialog = new()
				{
					MultiSelect = false
				};

				DialogResult response = openFileDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    statusMessage.Text += openFileDialog.FileName;
                }
                else if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                }

                openFileDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void FileSaveAction(bool isSaveAs=false)
        {
			try
			{
				SaveFileDialog saveFileDialog = new()
				{
					Title = isSaveAs ? "Save As..." : "Save..."
				};

				DialogResult response = saveFileDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    statusMessage.Text += saveFileDialog.FileName;
                }
                else if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                }

                saveFileDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void FileSaveAsAction()
        {
            FileSaveAction(true);
        }
        private void FilePrintAction()
        {
            string errorMessage = null;
			PrintSettings printer = null;//Eto.Forms.PrintSettings.FromArgb(0, 0, 0);
            if (GetPrinter(ref printer, ref errorMessage))
            {
                statusMessage.Text += printer.ToString();
                Console.WriteLine("PrintSettings:"+ printer);
                // Note: there is no actual output from printer.ToString()
            }
        }
        private static async Task FilePrintPreviewAction()
        {
            await DoSomething();
        }
        private void FileQuitAction
        (
            ref bool resultCancel
        )
        {
			statusMessage.Text = "Quit" + ACTION_INPROGRESS;
            StartProgressBar();
			// StartActionIcon("Quit"); 

			DialogResult response = MessageBox.Show
			(
				parent: this,
				text: "Quit?",
				caption: Title,
				buttons: MessageBoxButtons.YesNo,
				type: MessageBoxType.Question,
				defaultButton: MessageBoxDefaultButton.Yes
			);

			if (response == DialogResult.No)
            {
                resultCancel = true;

                // StopActionIcon(); 
                StopProgressBar();
                statusMessage.Text += "cancelled";//ACTION_DONE;
            }
        }
        private static async Task EditUndoAction()
        {
            await DoSomething();
        }
        private static async Task EditRedoAction()
        {
            await DoSomething();
        }
        private static async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private static async Task EditCutAction()
        {
            await DoSomething();
        }
        private static async Task EditCopyAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private static async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private static async Task EditFindAction()
        {
            await DoSomething();
        }
        private static async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private static async Task EditRefreshAction()
        {
            await DoSomething();
        }
        private /*async Task*/void EditPreferencesAction()
        {
			try
			{
				// await DoSomething();
				//do a folder-path dialog demo for preferences
				SelectFolderDialog selectFolderDialog = new()
				{
					Title = Title
				};

				DialogResult response = selectFolderDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    statusMessage.Text += selectFolderDialog.Directory;
                }
                else //if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                }

                selectFolderDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private static async Task WindowNewWindowAction()
        {
            await DoSomething();
        }
        private static async Task WindowTileAction()
        {
            await DoSomething();
        }
        private static async Task WindowCascadeAction()
        {
            await DoSomething();
        }
        private static async Task WindowArrangeAllAction()
        {
            await DoSomething();
        }
        private static async Task WindowHideAction()
        {
            await DoSomething();
        }
        private static async Task WindowShowAction()
        {
            await DoSomething();
        }
        private static async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private static async Task HelpIndexAction()
        {
            await DoSomething();
        }
        private static async Task HelpOnlineHelpAction()
        {
            await DoSomething();
        }
        private static async Task HelpLicenseInformationAction()
        {
            await DoSomething();
        }
        private static async Task HelpCheckForUpdatesAction()
        {
            await DoSomething();
        }
        private void HelpAboutAction()
        {
			AboutDialog aboutDialog = new()
			{
				// aboutDialog.Title = "About EtoFormsApp2";
				Logo = GetIconFromBitmap(GetBitmapFromFile("../EtoFormsApp2/Resources/App.png"), 32, 32),
				ProgramName = "EtoFormsApp2",
				Version = "0.0.3",
				ProgramDescription = "Desktop GUI app prototype, on Linux(/Win/Mac), in C# / DotNet[5+] / Gtk#(/WPF/AppKit), using VSCode / Eto.Forms",
				WebsiteLabel = "WebsiteLabel",
				Website = new Uri("http://www.gitlab.com"),
				// aboutDialog.Platform;r/o
				Copyright = "Copyright (C) 2022 Stephen J Sepan",
				Designers = ["Stephen J Sepan", "Designer2", "Designer3"],
				Developers = ["Stephen J Sepan", "Developer2", "Developer3"],
				Documenters = ["Stephen J Sepan", "Documenter2", "Documenter3"],
				License = "License text here"
			};
			_ = aboutDialog.ShowDialog(this);
		}
        private void ColorAction()
        {
            string errorMessage = null;
			Color color = Color.FromArgb(0, 0, 0);
            if (GetColor(ref color, ref errorMessage))
            {
                statusMessage.Text += color.ToString();
            }
        }
        private void FontAction()
        {
            string errorMessage = null;
			Font font = Fonts.Sans(10);
            if (GetFont(ref font, ref errorMessage))
            {
                statusMessage.Text += font.ToString();
            }
        }

        #endregion Actions

        #region utility
        private void StartProgressBar()
        {
            progressBar.Value = 33;
            progressBar.Indeterminate = true;
            progressBar.Visible = true;
            //DoEvents;
        }

        private void StopProgressBar()
        {
            //DoEvents;
            progressBar.Visible = false;
        }

        private void StartActionIcon(string resourceItemId)
        {
            actionIcon.Image = imageResources[resourceItemId]; //"New", etc.
            actionIcon.Visible = true;
        }

        private void StopActionIcon()
        {
            actionIcon.Visible = false;
            actionIcon.Image = imageResources["New"];
        }

        // https://www.hanselman.com/blog/how-do-you-use-systemdrawing-in-net-core
        // private static Image GetImageFromFile(string resourceItemPath)
        // {
        //     Image returnValue = null;
        //     using (FileStream pngStream = new FileStream(resourceItemPath, FileMode.Open, FileAccess.Read))
        //     {
        //         Bitmap image = new Bitmap(pngStream);
        //         returnValue = image;
        //     }
        //     return returnValue;
        // }
        private static Bitmap GetBitmapFromFile(string resourceItemPath)
        {
            Bitmap returnValue = null;
            using (FileStream pngStream = new(resourceItemPath, FileMode.Open, FileAccess.Read))
            {
                returnValue = new(pngStream);
            }
            return returnValue;
        }
        private static Icon GetIconFromBitmap(Bitmap bitmap, int width, int height)
        {
            return bitmap.WithSize(width, height);
        }

        private bool GetPrinter
        (
            ref PrintSettings printer,
            ref string errorMessage
        )
        {
            bool returnValue = false;
			PrintDialog printDialog = new();
			try
			{
				DialogResult response = printDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    printer = printDialog.PrintSettings;
                    returnValue = true;
                }
                else //if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                    returnValue = true;
                }

                printDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// </summary>
        /// <param name="color">ref Color</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool GetColor
        (
            ref Color color,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				ColorDialog colorDialog = new();

				DialogResult response = colorDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    color = colorDialog.Color;
                    returnValue = true;
                }
                else //if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                }

                colorDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }

        /// <summary>
        /// Get a font.
        /// </summary>
        /// <param name="font">ref Font</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool GetFont
        (
            ref Font font,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				// fontResponse = null;
				FontDialog fontDialog = new();

				DialogResult response = fontDialog.ShowDialog(this);

				if (response == DialogResult.Ok)
                {
                    font = fontDialog.Font;
                    returnValue = true;
                }
                else //if (response == DialogResult.Cancel)
                {
                    statusMessage.Text += " cancelled ";
                }

                fontDialog.Dispose();
            }
            catch (Exception ex)
            {
				errorMessage = ex.Message;
                Console.Error.WriteLine(ex.Message);
			}
            return returnValue;
        }
        #endregion utility

    }
}
