﻿using System;
using Eto.Forms;

namespace EtoFormsApp2.Wpf
{
	static class Program
	{
		[STAThread]
		public static int Main(string[] args)
		{
            int returnValue = -1;

            try
            {
				new Application(Eto.Platforms.Wpf).Run(new MainForm());

				returnValue = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Main: " + ex.Message);
            }
            return returnValue;
		}
	}
}
